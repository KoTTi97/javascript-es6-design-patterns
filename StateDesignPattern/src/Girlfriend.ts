import { Mood } from "./interface/Mood";
import { Angry } from "./moods/Angry";
import { Neutral } from "./moods/Neutral";
import { Happy } from "./moods/Happy";

export class Girlfriend
{
    private readonly angry: Mood;
    private readonly neutral: Mood;
    private readonly happy: Mood;

    private mood: Mood;

    constructor()
    {
        this.angry = new Angry(this);
        this.neutral = new Neutral(this);
        this.happy = new Happy(this);

        this.mood = this.neutral;
    }

    annoy(): string
    {
        return this.mood.annoy();
    }

    talk(): string
    {
        return this.mood.talk();
    }

    kiss(): string
    {
        return this.mood.kiss();
    }

    getAngryMood(): Mood
    {
        return this.angry;
    }

    getNeutralMood(): Mood
    {
        return this.neutral;
    }

    getHappyMood(): Mood
    {
        return this.happy;
    }

    getMoodAsString(): string
    {
        return this.mood.getMoodAsString();
    }

    setMood(mood: Mood): void
    {
        this.mood = mood;
    }
}
