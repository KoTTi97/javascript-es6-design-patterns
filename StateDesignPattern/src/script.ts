import { Girlfriend } from "./Girlfriend";
import "../public/Präsentation.pdf";
import "../public/img/Logo.png";

/* =============== Interactions with girlfriend =============== */

function annoyGirlfriend(girlfriend: Girlfriend): void
{
    const response = girlfriend.annoy();
    updateLabels(girlfriend, response);
}

function talkToGirlfriend(girlfriend: Girlfriend): void
{
    const response = girlfriend.talk();
    updateLabels(girlfriend, response);
}

function kissGirlfriend(girlfriend: Girlfriend): void
{
    const response = girlfriend.kiss();
    updateLabels(girlfriend, response);
}


/* =============== Update UI after interaction with girlfriend =============== */

function updateLabels(girlfriend: Girlfriend, response: string): void
{
    updateGirlfriendMood(girlfriend);
    updateGirlfriendResponse(response);
}

function updateGirlfriendResponse(response: string): void
{
    $("#girlfriend-response").text("'" + response + "'");
}

function updateGirlfriendMood(girlfriend: Girlfriend): void
{
    const mood = girlfriend.getMoodAsString();
    $("#current-mood").text(mood);
    changeMoodImage(mood.toLowerCase() + "-girl");
}


/* =============== Setup button click handlers =============== */

function changeMoodImage(imageID: string): void
{
    $(".mood-image").css("display", "none");
    $("#" + imageID).css("display", "block");
}


/* =============== Setup button click handlers =============== */

$("#annoy-girlfriend").click((): void =>
{
    annoyGirlfriend(girlfriend);
});

$("#talk-to-girlfriend").click((): void =>
{
    talkToGirlfriend(girlfriend);
});

$("#kiss-girlfriend").click((): void =>
{
    kissGirlfriend(girlfriend);
});


/* =============== Execute on startup =============== */

const girlfriend: Girlfriend = new Girlfriend();

updateGirlfriendMood(girlfriend);
$("#girlfriend-response").text("'Hi'");

