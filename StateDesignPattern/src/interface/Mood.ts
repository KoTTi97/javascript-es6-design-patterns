export interface Mood
{
    annoy(),
    talk(),
    kiss(),
    getMoodAsString()
}
