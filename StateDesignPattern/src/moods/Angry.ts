import { Mood } from "../interface/Mood";
import { Girlfriend } from "../Girlfriend";

export class Angry implements Mood
{
    private girlfriend: Girlfriend;
    private moodString: string = "angry";

    constructor(girlfriend: Girlfriend)
    {
        this.girlfriend = girlfriend;
    }

    annoy(): string
    {
        // Girlfriend mood does not change
        console.log("Annoy angry girlfriend - 'Stop annoying me!' - Girlfriend stays angry");
        return "Stop annoying me!";
    }

    talk(): string
    {
        // Girlfriend mood does not change
        console.log("Talk to angry girlfriend - 'No, I am angry, I don't wan't to talk to you now' - Girlfriend stays angry");
        return "No, I am angry, I don't wan't to talk to you now";
    }

    kiss(): string
    {
        console.log("Kiss angry girlfriend - 'Okay, now I like you again' - Switch mood from angry to neutral");
        this.girlfriend.setMood(this.girlfriend.getNeutralMood());
        return "Okay, now I like you again";
    }

    getMoodAsString(): string
    {
        return this.moodString;
    }
}
