import { Mood } from "../interface/Mood";
import { Girlfriend } from "../Girlfriend";

export class Neutral implements Mood
{
    private girlfriend: Girlfriend;
    private moodString: string = "neutral";

    constructor(girlfriend: Girlfriend)
    {
        this.girlfriend = girlfriend;
    }

    annoy(): string
    {
        console.log("Annoy neutral girlfriend - 'You're a pain in the neck!' - girlfriend gets angry");
        this.girlfriend.setMood(this.girlfriend.getAngryMood());
        return "You're a pain in the neck!";
    }

    talk(): string
    {
        // Girlfriend mood does not change
        console.log("Talk to neutral girlfriend - 'Bla bla bla' - girlfriend stays in neutral mood");
        return "Yesterday my friend Gertrud did ... ...";
    }

    kiss(): string
    {
        console.log("Kiss neutral girlfriend - 'Hihi' - girlfriend gets happy");
        this.girlfriend.setMood(this.girlfriend.getHappyMood());
        return "Hihi";
    }

    getMoodAsString(): string
    {
        return this.moodString;
    }
}
