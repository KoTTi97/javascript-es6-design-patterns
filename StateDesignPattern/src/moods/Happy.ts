import { Mood } from "../interface/Mood";
import { Girlfriend } from "../Girlfriend";

export class Happy implements Mood
{
    private girlfriend: Girlfriend;
    private moodString: string = "happy";

    constructor(girlfriend: Girlfriend)
    {
        this.girlfriend = girlfriend;
    }

    annoy(): string
    {
        console.log("Annoy happy girlfriend - 'You're a pain in the neck!' - girlfriend gets angry");
        this.girlfriend.setMood(this.girlfriend.getAngryMood());
        return "You're a pain in the neck!";
    }

    talk(): string
    {
        // Girlfriend mood does not change
        console.log("Talk to happy girlfriend - 'Bla bla bla' - girlfriend stays happy");
        return "Hey did you know ... ...";
    }

    kiss(): string
    {
        // Girlfriend mood does not change
        console.log("Kiss happy girlfriend - 'Hihi' - girlfriend stays happy");
        return "Hihi";
    }

    getMoodAsString(): string
    {
        return this.moodString;
    }
}
