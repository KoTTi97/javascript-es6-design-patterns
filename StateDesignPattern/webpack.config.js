const HTMLWebpackPlugin = require("html-webpack-plugin");

module.exports = {
    entry: "./src/script.ts",
    output: {
        filename: "bundle.js",
        path: __dirname + "/dist"
    },
    devServer: {
        port: 3000,
        hot: true,
        open: true,
        inline: true,
        contentBase: ['./dist', './public', './src'],
        watchContentBase: true
    },
    resolve: {
        extensions: [".ts", ".js"]
    },
    module: {
        rules: [
            {
                test: /\.ts?$/,
                loader: "ts-loader"
            },
            {
                test: /\.html$/,
                use: ["html-loader"]
            },
            {
                test: /\.pdf$/,
                use: [
                    {
                        loader: "file-loader",
                        options: {
                            name: "[name].[ext]",
                            outputPath: "/",
                            publicPath: "/"
                        }
                    }
                ]
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/,
                use: [
                    {
                        loader: "file-loader",
                        options: {
                            name: "[name].[ext]",
                            outputPath: "img/",
                            publicPath: "img/"
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        new HTMLWebpackPlugin({
            template: "./public/index.html"
        }),
    ]
};
